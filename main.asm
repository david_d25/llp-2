section .data
welcome_msg: db 'Please, type your word:', 10, '>>> ', 0
key_too_long_msg: db 'Sorry, this key is too long (255 max)', 10, 0
key_msg: db 'Key: ', 0
value_msg: db 'Value: ', 0
no_value_msg: db 'Value not found', 10, 0

%include "colon.inc"
%include "words.inc"

section .text
global _start

extern exit
extern string_length
extern print_string
extern print_err_string
extern print_char
extern print_newline
extern print_uint
extern print_int
extern string_equals
extern read_char
extern read_word
extern parse_uint
extern parse_int
extern string_copy

extern find_word

_start:
    mov rdi, welcome_msg
    call string_length
    mov rdi, welcome_msg
    call print_string

    sub rsp, 255
    mov rdi, rsp
    mov rsi, 255
    call read_word ; rax - address, rdx - length
    test rax, rax
    jz .key_long

    .key_ok:
        mov rdi, rsp
        mov rsi, next
        call find_word
        test rax, rax
        jz .not_found

        .found:
            push rax
            mov rdi, key_msg
            call print_string
            pop rax
            mov rdi, rsp
            push rax
            call print_string
            call print_newline
            mov rdi, value_msg
            call print_string
            pop rax
            mov rdi, rax
            call print_string
            call print_newline
            jmp .exit

        .not_found:
            mov rdi, no_value_msg
            call print_err_string
            jmp .exit

    .key_long:
        mov rdi, key_too_long_msg
        call print_err_string
        jmp .exit

    .exit:
        add rsp, 255
        call exit