global exit;:function ?
global string_length
global print_string
global print_err_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .data
number_chars: db '0123456789'

section .text
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    push r13
    xor r13, r13
    .loop:
        cmp byte[rdi+r13], 0
        jz .exit
        inc r13
        jmp .loop
    .exit:
        mov rax, r13
        pop r13
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax ; length
    mov rsi, rdi ; buffer
    mov rdi, 1 ; stdout
    mov rax, 1 ; 'write'
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_err_string:
    call string_length
    mov rdx, rax ; length
    mov rsi, rdi ; buffer
    mov rdi, 2 ; stderr
    mov rax, 1 ; 'write'
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1 ; length
    mov rdi, 1 ; stdout
    lea rsi, [rsp]
    mov rax, 1 ; 'write'
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r8, rsp
    push 0
    mov r10, 10

    .loop:
        xor rdx, rdx
        div r10
        mov r11, [number_chars + rdx]
        and r11, 0xFF
        shl r11, 56
        push r11
        add rsp, 7
        test rax, rax
        jnz .loop

    .write:
        mov rdi, rsp
        call print_string
        mov rsp, r8
    .exit:
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    push rbx
    cmp rdi, 0
    jl .negative
    jmp .print
    .negative:
        neg rdi
        mov rbx, rdi
        mov rdi, '-' ; minus char
        call print_char
        mov rdi, rbx
    .print:
        call print_uint
    pop rbx
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
        mov r8b, byte [rdi]
        mov r9b, byte [rsi]
        cmp r8b, r9b
        jnz .neq
        test r8b, r8b
        jz .eq
        inc rdi
        inc rsi
        jmp .loop
    .eq:
        mov rax, 1
        ret
    .neq:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rbx
    push 0
    mov rax, 0 ; 'read'
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rbx
    test rbx, rbx
    jz .eof
    mov rax, rbx
    pop rbx
    ret

    .eof:
        xor rax, rax
        pop rbx
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    mov r12, rdi
    mov r13, rsi
    
    dec rsi ; reserved one byte for null-terminator

    cmp rsi, 0 ; is there enough space?
    jle .exit_ok
    jmp .loop

    .is_whitespace:
        cmp rax, 0x20 ; is this space?
        jz .yes
        cmp rax, 0x9 ; is this tab?
        jz .yes
        cmp rax, 0xA ; is this \n?
        jz .yes
        .no:
            mov rdx, 0
            ret
        .yes:
            mov rdx, 1
            ret

    .loop:
        push rdi
        push rsi
        call read_char
        pop rsi
        pop rdi
        call .is_whitespace
        cmp rdi, r12 ; are we at the start of the buffer?
        jnz .write
        test rdx, rdx
        jnz .loop ; this is word-beginning whitespace, skip it

        .write:
            test rax, rax
            jz .exit_ok ; EOF

            test rsi, rsi
            jz .exit_fail ; buffer overflow, return 0

            test rdx, rdx
            jnz .exit_ok ; this is word-ending whitespace, end writing

            mov [rdi], rax ; write this char
            inc rdi
            dec rsi
            jmp .loop

    .exit_ok:
        mov [rdi], byte 0 ; add null-terminator
        mov rax, r12
        mov rdx, r13
        sub rdx, rsi
        dec rdx
        jmp .return

    .exit_fail:
        xor rax, rax

    .return:
        pop r13
        pop r12
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    mov r10, 10
    .loop:
        mov cl, byte [rdi]
        test cl, cl
        jz .exit
        sub rcx, 48 ; symbol to number
        jl .exit
        cmp rcx, 9
        jg .exit
        push rdx
        xor rdx, rdx
        mul r10
        pop rdx
        add rax, rcx
        inc rdx
        inc rdi
        jmp .loop
    .exit:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov r8b, byte [rdi]
    cmp r8b, '-'
    jz .negative
    jmp .positive
    .negative:
        inc rdi
        call parse_int
        neg rax
        inc rdx
        jmp .exit
    .positive:
        jmp parse_uint
    .exit:
        ret 

; Принимает указатель на строку (rdi), указатель на буфер (rsi) и длину буфера (rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        cmp rdx, rax
        jl .exit0

        mov r11b, byte [rdi + rax]
        mov byte [rsi + rax], r11b

        cmp byte [rdi + rax], 0
        jz .exit

        inc rax
        jmp .loop
    .exit0:
        xor rax, rax
        ret

    .exit:
        ret
