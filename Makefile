all: dict.o lib.o main.o
	ld -o app dict.o lib.o main.o 

dict.o: dict.asm lib.asm
	nasm -felf64 dict.asm

lib.o: lib.asm 
	nasm -felf64 lib.asm

main.o: main.asm lib.asm dict.asm words.inc colon.inc
	nasm -felf64 main.asm

clean:
	rm -f *.o *.out app
