global find_word
extern string_equals
extern string_length

find_word:
    mov r10, rdi ; buf*
    mov r11, rsi ; last_word*

    .loop:
        mov rdi, r10
        mov rsi, r11
        add rsi, 8
        call string_equals
        test rax, rax
        jnz .found
        mov r9, [r11]
        test r9, r9
        jz .exit
        mov r11, [r11]
        jmp .loop
        .found:
            mov rdi, r11
            add rdi, 8 
            call string_length
            add rax, r11
            add rax, 9 ; 8 + 1
            ret
    .exit:
        xor rax, rax
        ret